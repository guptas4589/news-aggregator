package com.skg.news_aggregator.news;

import com.skg.news_aggregator.publisher.Publisher;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

@Repository
@Transactional
public class NewsRepository {
    @PersistenceContext
    EntityManager entityManager;

    public News save(News news) {
        return entityManager.merge(news);
    }

    public News save(URL url, String description, String content, Publisher publisher) {
        News news = new News();
        news.setUrl(url);
        news.setContent(content);
        news.setDescription(description);
        news.setPublisher(publisher);
        return save(news);
    }

    public News find(Long id) {
        if(id != null) {
            return entityManager.find(News.class, id);
        }
        return null;
    }

    public List<News> findAll() {
        Query query = entityManager.createQuery("SELECT n FROM news n");
        return (List<News>) query.getResultList();
    }

    public News findByUrl(String urlString) {
        try {
            URL url = new URL(urlString);
            Query query = entityManager.createQuery("SELECT distinct(n) FROM news n where n.url = :newsUrl")
                    .setParameter("newsUrl", url);
            try {
                return (News) query.getSingleResult();
            } catch (NoResultException e) {
//                ignore
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        return null;
    }
}
