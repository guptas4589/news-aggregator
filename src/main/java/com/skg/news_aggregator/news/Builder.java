package com.skg.news_aggregator.news;

import java.util.function.Consumer;

public class Builder<T> {

    final private T object;
    
    public Builder(T newInstance) {
        this.object = newInstance;
    }
    
    public Builder<T> with(Consumer<T> consumer) {
        consumer.accept(object);
        return this;
    }
    
    public T create() {
        return object;
    }
}