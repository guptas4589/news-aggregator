package com.skg.news_aggregator.news;

import com.skg.news_aggregator.feed.Feed;
import com.skg.news_aggregator.feed.FeedNewsExtractorService;
import com.skg.news_aggregator.publisher.Publisher;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.net.URL;
import java.util.List;

@Service
@Transactional
public class NewsService {

    @Autowired
    NewsRepository newsRepository;

    @Autowired
    FeedNewsExtractorService feedNewsExtractorService;

    @Autowired
    NewsIndexService newsIndexService;

    private static Logger logger = LogManager.getLogger();

    public News findByUrl(String url) {
        return newsRepository.findByUrl(url);
    }

    public News findById(Long id) {
            return newsRepository.find(id);
    }

    public List<News> findAll() {
        return newsRepository.findAll();
    }

    public boolean exists(String url) {
        return newsRepository.findByUrl(url) != null;
    }

    public News save(News news) {
        return newsRepository.save(news);
    }

    public News save(URL url, String description, String content, Publisher publisher) {
        return newsRepository.save(url, description, content, publisher);
    }

    public void updateNews(List<Feed> feeds) {
        feeds.forEach((feed) -> {
            try {
                List<News> newsList = feedNewsExtractorService.createNews(feed);

                newsList.forEach((news -> {
                    News savedNews = save(news);
                    if(savedNews != null) {
                        logger.info("#saved new" + savedNews.getId() + ":" + savedNews.getTitle());
                        try {
                            newsIndexService.addToIndex(news);
                        } catch (IOException e) {
                            logger.warn("#addToIndex failed to add news to index. news", news, "\n Exception:" + e);
                        }
                    }
                }));
            } catch (Exception e) {
                logger.warn("#updateNews failed to retrieve feed for" + feed.getId() +":" +feed.getUrl());
            }
        });
    }

    public void addToIndex(List<News> newsList) throws IOException {
        newsIndexService.addToIndex(newsList);
    }
}
