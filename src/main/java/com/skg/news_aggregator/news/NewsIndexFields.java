package com.skg.news_aggregator.news;

public class NewsIndexFields {
    public static final String DESCRIPTION = "description";
    public static final String SOURCE = "source";
    public static final String CONTENT = "content";
    public static final String TITLE = "title";
}
