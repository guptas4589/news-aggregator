package com.skg.news_aggregator.news;

import com.skg.news_aggregator.index.Index;
import org.apache.lucene.search.Sort;

public class IndexSearchQuery {
    public Index index;
    public String[] fieldNames;
    public String queryString;
    public Integer max;
    public Sort sort;
    public boolean getMaxScore;
    public boolean getDocScore;
    public Integer minScorePercent;
}
