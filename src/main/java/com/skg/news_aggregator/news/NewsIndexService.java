package com.skg.news_aggregator.news;

import com.skg.news_aggregator.index.Index;
import com.skg.news_aggregator.index.LuceneSearch;
import com.skg.news_aggregator.search.InvalidQueryException;
import com.skg.news_aggregator.search.SearchFailedException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.IndexableField;
import org.apache.lucene.index.IndexableFieldType;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.search.Sort;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.ServletContext;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Service
public class NewsIndexService {

    @Autowired
    ServletContext servletContext;

    Logger logger = LogManager.getLogger();

    @Autowired
    NewsService newsService;

//    TODO: move to property file
    public static final String indexName = "newsIndex";
    public static final Integer searchScorePercentThreshold = 10;
    public static final Integer similarityPercentThreshold = 20;

    private static final String[] searchFields = { NewsIndexFields.TITLE, NewsIndexFields.SOURCE,
            NewsIndexFields.DESCRIPTION };
    private static final String[] similarityFilterFields = {NewsIndexFields.TITLE, NewsIndexFields.SOURCE,
            NewsIndexFields.DESCRIPTION , NewsIndexFields.CONTENT};

    private Document createNewsDocument(News news) throws IOException{
        IndexableFieldType fieldType = TextField.TYPE_STORED;
        Document document = getIndex().createDocument(NewsIndexFields.TITLE, news.getTitle(), fieldType);

        Field field = new Field(NewsIndexFields.DESCRIPTION, news.getDescription(), fieldType);
        document.add(field);
        field = new Field(NewsIndexFields.SOURCE, news.getUrl().toString(), fieldType);
        document.add(field);
        field = new Field(NewsIndexFields.CONTENT, news.getContent(), fieldType);
        document.add(field);

        return document;
    }

    public void addToIndex(News news) throws IOException {
        getIndex().indexDoc(createNewsDocument(news));
    }

    public void addToIndex(List<News> newsList) throws IOException {
        Index index = getIndex();
        List<Document> documentList = new ArrayList<>();
        newsList.forEach((news) -> {
            try {
                documentList.add(createNewsDocument(news));
            } catch (IOException e) {
                logger.warn("#addToIndex failed to create document for news {} to index. Error {}",
                        news.id, e.getMessage());
            }
        });
        index.indexDocuments(documentList);
    }

    public List<String> search(String queryString) throws IOException, ParseException {
        Index index = getIndex();
        IndexSearchQuery indexSearchQuery = new Builder<> (new IndexSearchQuery())
                .with($ -> {
                    $.index =getIndex();
                    $.queryString = queryString;
                    $.fieldNames = searchFields;
                    $.max = 50;
                    $.sort = Sort.RELEVANCE;
                }).create();
        List<Document> documents = LuceneSearch.getDocuments(indexSearchQuery);
        logger.debug("#searchNews search resulted in ", documents.size(), " document matches.");
        List<String> newsUrls = new ArrayList<>();
        documents.forEach(document -> {
            IndexableField field = document.getField(NewsIndexFields.SOURCE);
            if(field != null) {
                String urlString = field.stringValue();
                logger.debug("#search search matches with news url:", urlString);
                newsUrls.add(urlString);
            } else {
                logger.debug("#search document does not contain specified field:");
            }
        });
        return newsUrls;
    }

    public List<String> getSimilarNewsUrls(String newsUrlString) throws SearchFailedException, InvalidQueryException {
        List<String> newsUrlList;
        String[] similarSearchFields = new String[]{NewsIndexFields.TITLE, NewsIndexFields.SOURCE, NewsIndexFields.DESCRIPTION , NewsIndexFields.CONTENT};
        String newsUrlFieldName = NewsIndexFields.SOURCE;
        try {
            Integer docId = LuceneSearch.searchExact(newsUrlString, newsUrlFieldName, newsUrlString, getIndex());
            newsUrlList = getSimilarNewsSourceUrlList(docId, similarSearchFields );
        }catch (IOException e) {
            SearchFailedException exception = new SearchFailedException();
            exception.initCause(e);
            throw exception;
        } catch (ParseException e) {
            InvalidQueryException exception = new InvalidQueryException();
            exception.initCause(e);
            throw exception;
        }
        return newsUrlList;
    }

    public Map<String, List<String>> searchAndFindSimilar(String queryString) throws SearchFailedException, InvalidQueryException {
        Index index = getIndex();
        List<Integer> docIds;
        IndexSearchQuery indexSearchQuery = new Builder<> (new IndexSearchQuery())
                .with($ -> {
                    $.index =getIndex();
                    $.queryString = queryString;
                    $.fieldNames = searchFields;
                    $.max = 50;
                    $.getDocScore = true;
                    $.getMaxScore = true;
                    $.sort = Sort.RELEVANCE;
                    $.minScorePercent = searchScorePercentThreshold;
                }).create();

        try {
            docIds = LuceneSearch.getDocIds(indexSearchQuery);
            return getUrlsAndSimilarNewsUrlMap(docIds, similarityFilterFields);
        } catch (IOException e) {
            SearchFailedException exception = new SearchFailedException();
            exception.initCause(e);
            throw exception;
        } catch (ParseException e) {
            InvalidQueryException exception = new InvalidQueryException();
            exception.initCause(e);
            throw exception;
        }
    }

    private Map<String, List<String>> getUrlsAndSimilarNewsUrlMap(List<Integer> docIds, String[] similarityFilterFields) {
        Map<String, List<String>> urlsAndSimilarNewsUrlMap = new HashMap<>();

        for(Integer docId: docIds) {
            try {
                Document document = LuceneSearch.getDocument(docId, getIndex().getSearcher());
                IndexableField mainDocField = document.getField(NewsIndexFields.SOURCE);
                if(mainDocField != null) {
                    String newsUrl = mainDocField.stringValue();
                    logger.info("#searchAndFindSimilar searching similar for:" + mainDocField + "docId:" + docId);
                    List<String> similarNewsUrlList = getSimilarNewsSourceUrlList(docId, similarityFilterFields);

                    urlsAndSimilarNewsUrlMap.put(newsUrl,similarNewsUrlList);
                }
            } catch (IOException e) {
                logger.warn("#getUrlsAndSimilarNewsUrlMap failed while searching for similar news for docId", docId);
                // ignore if error finding the similar news for any document.
            }
        }
        return urlsAndSimilarNewsUrlMap;
    }

    private List<String> getSimilarNewsSourceUrlList(int docId, String[] fields) {
        List<Document> similarDocuments = findSimilar(docId, fields);
        List<String> similarDocSourceValues = new ArrayList<>();
        for(Document d: similarDocuments) {
            IndexableField indexableField = d.getField(NewsIndexFields.SOURCE);
            if(indexableField != null) {
                similarDocSourceValues.add(indexableField.stringValue());
            }
        }
        return similarDocSourceValues;
    }

    private List<Document> findSimilar(Integer docId, String[] fields) {
        List<Document> documents = new ArrayList<>();
        try {
            documents = LuceneSearch.getMoreLikeThis(getIndex(), docId, fields, similarityPercentThreshold);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return documents;
    }
    public Index getIndex() {
        return (Index) servletContext.getAttribute(indexName);
    }
}
