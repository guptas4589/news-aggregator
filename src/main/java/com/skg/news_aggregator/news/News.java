package com.skg.news_aggregator.news;


import com.skg.news_aggregator.publisher.Publisher;
import org.hibernate.annotations.Type;
import org.hibernate.type.UrlType;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.net.URL;
import java.util.Date;

@Entity(name = "news")
public class News {

    @Id @GeneratedValue
    Long id;

    @org.hibernate.annotations.Type( type="java.net.URL" )
    @Column(unique = true)
    URL url;

    String title;

    @NotNull @Type(type = "text")
    String content;

    @NotNull @Length(max = 1000)
    String description;

    Date date;

    @ManyToOne
//    @JoinColumn(name = "publisher_id")
    Publisher publisher;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Publisher getPublisher() {
        return publisher;
    }

    public void setPublisher(Publisher publisher) {
        this.publisher = publisher;
    }

    public URL getUrl() {
        return url;
    }

    public void setUrl(URL url) {
        this.url = url;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public String toString() {
        return "Id:" + getId() + " url:" + getUrl() + " title" + getTitle();
    }
}
