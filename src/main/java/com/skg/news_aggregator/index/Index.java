package com.skg.news_aggregator.index;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.index.*;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.store.Directory;

import java.io.IOException;
import java.util.List;

public class Index {
    Analyzer analyzer;
    Directory directory;
    private IndexSearcher indexSearcher;
    private DirectoryReader directoryReader;
    private IndexWriter indexWriter;

    public Index(Analyzer analyzer, Directory directory) throws IOException {
        this.analyzer = analyzer;
        this.directory = directory;
//      Write to index in order to get reader. Index writer may be re-used if writing to index frequently.
        this.indexWriter = createWriter();
        indexWriter.commit();
        directoryReader = DirectoryReader.open(directory);
        indexSearcher = new IndexSearcher(directoryReader);
    }

    private IndexWriter createWriter() throws IOException {
        return new IndexWriter(directory, new IndexWriterConfig(analyzer));
    }

    public void indexDoc(Document document) throws IOException {
        indexWriter.addDocument(document);
        indexWriter.commit();
    }

    /**
     * Puts multiple document to index. Recommended to use this method if many documents needs to be indexed in sequence
     * since this will reuse the same index write or else you are responsible to make sure you don't unnecessarily creating
     * and closing index writers.
     *
     * @param documents - {@link List} of {@link Document}
     * @throws IOException
     */
    public void indexDocuments(List<Document> documents) throws IOException {
        indexWriter.addDocuments(documents);
        indexWriter.commit();
    }

    public Document createDocument(String fieldName, String textContent, IndexableFieldType type) throws IOException {
        Document document = new Document();
        document.add(new Field(fieldName, textContent, type));
        return document;
    }

    public DirectoryReader getReader() throws IOException {
        DirectoryReader newDirectoryReader = DirectoryReader.openIfChanged(directoryReader);
        if(newDirectoryReader != null) {   // index updated, use new directory reader
            directoryReader = newDirectoryReader;
        }
        return directoryReader;
    }

    public IndexSearcher getSearcher() throws IOException {
        DirectoryReader newDirectoryReader = DirectoryReader.openIfChanged(directoryReader);
        if(newDirectoryReader != null) {
            indexSearcher = new IndexSearcher(getReader());
        }
        return indexSearcher;
    }

    @Override
    protected void finalize() throws Throwable {
        super.finalize();
        this.indexWriter.commit();
        this.indexWriter.close();
        this.directoryReader.close();
    }
}
