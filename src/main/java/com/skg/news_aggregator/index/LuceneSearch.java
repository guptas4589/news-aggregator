package com.skg.news_aggregator.index;

import com.skg.news_aggregator.news.IndexSearchQuery;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.Document;
import org.apache.lucene.queries.mlt.MoreLikeThis;
import org.apache.lucene.queryparser.classic.MultiFieldQueryParser;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

public class LuceneSearch {

    private static final Logger logger = LogManager.getLogger();

    public static List<Document> getDocuments(IndexSearchQuery queryObj) throws IOException, ParseException {
        MultiFieldQueryParser parser = new MultiFieldQueryParser(
                queryObj.fieldNames,
                queryObj.index.analyzer);
        return getDocuments(parser, queryObj);
    }

    public static List<Integer> getDocIds(IndexSearchQuery indexSearchQuery) throws IOException, ParseException {
        MultiFieldQueryParser parser = new MultiFieldQueryParser(
                indexSearchQuery.fieldNames,
                indexSearchQuery.index.analyzer);
        TopDocs topDocs = topDocs(parser, indexSearchQuery);
        return getDocIds(topDocs, indexSearchQuery.minScorePercent);
    }

    public static List<Integer> getDocIds(ScoreDoc[] hits) {
        List<Integer> docIds = new ArrayList<>();
        for(ScoreDoc hit: hits) {
            docIds.add(hit.doc);
        }
        return docIds;
    }
    public static List<Integer> getDocIds(TopDocs topDocs, Integer minScorePercentage) {
        ScoreDoc[] hits = topDocs.scoreDocs;
        List<Integer> docIds = new ArrayList<>();
        Float scorePercent;
        logger.debug("#getDocIds hits size {}", hits.length);
        for(ScoreDoc hit: hits) {
            scorePercent = hit.score / topDocs.getMaxScore() * 100;
            logger.debug("#getDocIds socre {} score percent {} minScorPercent {}", hit.score, scorePercent, minScorePercentage);
            if(scorePercent > minScorePercentage) {
                docIds.add(hit.doc);
            }
        }
        return docIds;
    }
/*    public static List<Document> getDocuments(IndexSearchQuery queryObj) throws IOException, ParseException {
        QueryParser parser = new QueryParser(fieldName, index.analyzer);
        return getDocuments(parser, queryString, index, max, sort);

    }*/

    public static List<Document> getDocuments(QueryParser parser, IndexSearchQuery queryObj) throws IOException, ParseException {
        IndexSearcher searcher = queryObj.index.getSearcher();
        String escapedQueryString = parser.escape(queryObj.queryString);
        Query query = parser.parse(escapedQueryString);
        ScoreDoc[] hits = searcher.search(query, queryObj.max, queryObj.sort).scoreDocs;
        List<Document> documents =getDocuments(hits, searcher);
        return documents;
    }

    public static Integer searchExact(String queryString, String fieldName, String fieldValue, Index index) throws IOException, ParseException {
        Optional<Integer> firstMatchId = getDocumentsWithExactMatch(queryString,fieldName, index, 1, Sort.RELEVANCE)
                .stream().findFirst();
        Integer docId = -1;
        if(firstMatchId.isPresent()) {
            docId = firstMatchId.get();
            Document doc = getDocument(docId, index.getSearcher());
            String docFieldValue = doc.getField(fieldName).stringValue();
            if(!fieldValue.equals(docFieldValue)) {
                docId = -1;
            }
        }
        return docId;
    }

    public static List<Integer> getDocumentsWithExactMatch(String phrasedQueryString, String fieldName, Index index, int max, Sort sort) throws IOException, ParseException {
        logger.debug("#getDocumentsWithExactMatch queryPhrase: {}, field: {}, max: {}, sort: {}", phrasedQueryString, fieldName, max, sort);
        IndexSearcher searcher = index.getSearcher();
        QueryParser parser = new QueryParser(fieldName, index.analyzer);
        String escapedQueryString = parser.escape(phrasedQueryString);
        Query query = parser.parse("+(" + escapedQueryString  + ")");

//        PhraseQuery phraseQuery = new PhraseQuery(0, field, );
//        Query query = parser.parse(phraseQuery);
        ScoreDoc[] hits = searcher.search(query, max, sort).scoreDocs;
        logger.debug("#getDocumentsWithExactMatch hit size {}", hits.length);
        List<Integer> docIds = getDocIds(hits);
        return docIds;
    }

    public static TopDocs topDocs(QueryParser parser, IndexSearchQuery queryObj) throws IOException, ParseException {
        IndexSearcher searcher = queryObj.index.getSearcher();
        String escapedQueryString = parser.escape(queryObj.queryString);
        Query query = parser.parse(escapedQueryString);
        return searcher.search(query, queryObj.max, queryObj.sort, true, true);
    }

    public static List<Document> getMoreLikeThis(Index index, int docId, String[] fields, Integer minScorePercentage) throws IOException, ParseException {
        IndexSearcher searcher = index.getSearcher();

        MoreLikeThis mlt = new MoreLikeThis(index.getReader());
        mlt.setMinTermFreq(2);
        mlt.setMinDocFreq(0);
        mlt.setFieldNames(fields);
        mlt.setAnalyzer(new StandardAnalyzer());

        Query query = mlt.like(docId);
        TopDocs topDocs = searcher.search(query, 20, Sort.RELEVANCE, true, true);
        Float maxScore = topDocs.getMaxScore();
        List<Document> similarDocs = new ArrayList<>();
        Arrays.stream(topDocs.scoreDocs).forEach(scoreDoc -> {
            Float scorePercent = scoreDoc.score / maxScore * 100;
            try {
                if(scorePercent > minScorePercentage) {
                    similarDocs.add(searcher.doc(scoreDoc.doc));
                }
            } catch (IOException e) {
                logger.warn("#getMoreLikeThis {}", e.getMessage());
            }
        });

        return similarDocs;
    }

    public static List<Document> getDocuments(ScoreDoc[] hits, IndexSearcher searcher) throws IOException {
        List<Document> documents = new ArrayList<>();
        for (int i = 0; i < hits.length; i++) {
            int docId = hits[i].doc;
            documents.add(searcher.doc(docId));
        }
        return documents;
    }

    public static Document getDocument(Integer docId, IndexSearcher indexSearcher) throws IOException {
        return indexSearcher.doc(docId);
    }
}
