package com.skg.news_aggregator.search;

import com.skg.news_aggregator.news.News;

import java.util.List;

public class SimilarNewsInfo {
    News news;
    List<News> similarNews;
}
