package com.skg.news_aggregator.search;

import java.util.ArrayList;
import java.util.List;

public class NewsSearchResult {
    List<SearchResultEntry> entry = new ArrayList<>();

    public List<SearchResultEntry> getEntry() {
        return entry;
    }

    public void setEntry(List<SearchResultEntry> entry) {
        this.entry = entry;
    }
}
