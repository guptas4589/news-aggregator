package com.skg.news_aggregator.search;

import com.skg.news_aggregator.news.NewsIndexService;
import com.skg.news_aggregator.news.News;
import com.skg.news_aggregator.news.NewsService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.lucene.queryparser.classic.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service
public class NewsSearchService {

    @Autowired
    NewsIndexService newsIndexService;

    @Autowired
    NewsService newsService;

    private static Logger logger = LogManager.getLogger();


    public NewsSearchResult searchNewsAndFindSimilar(String queryString) throws SearchFailedException, InvalidQueryException{
        Map<String, List<String>> newsSourceUrlsAndSimilarUrls = newsIndexService.searchAndFindSimilar(queryString);
        logger.debug("#searchNewsAndFindSimilar {} matches for {} from index", newsSourceUrlsAndSimilarUrls.size(), queryString);

        NewsSearchResult searchResult = new NewsSearchResult();

        newsSourceUrlsAndSimilarUrls.entrySet().forEach((entry) -> {
            String mainNewsUrl = entry.getKey();
            News mainNews = newsService.findByUrl(mainNewsUrl);
            if (mainNews != null) {
                List<News> newsList = getSimilarNewsList(mainNews, entry.getValue());
                SearchResultEntry resultEntry = new SearchResultEntry();
                resultEntry.news = mainNews;
                resultEntry.similarNewsCount = newsList.size();
                searchResult.entry.add(resultEntry);
            }
        });
        return searchResult;
    }

    public List<News> getSimilarNews(Long id)  throws SearchFailedException, InvalidQueryException {
        logger.debug("#getSimilarNews main news news Id: {}", id);
        News news = newsService.findById(id);
        if(news != null) {
            List<String> similarNewsUrls = newsIndexService.getSimilarNewsUrls(news.getUrl().toString());
            return getSimilarNewsList(news, similarNewsUrls);
        } else {
            return new ArrayList<>();
        }
    }

    private List<News> getSimilarNewsList(News news, List<String> similarNewsUrls) {
        logger.debug("#getSimilarNewsList main news url" + news.getUrl());
        List<News> similarNewsList = new ArrayList<>();

        if(similarNewsUrls != null) {
            similarNewsUrls.forEach((newsUrl) -> {
                News similarNews = newsService.findByUrl(newsUrl);

                if(similarNews != null && !similarNews.getUrl().toString().equals(news.getUrl().toString())) {
                    logger.debug("#getSimilarNewsList similar news url" + similarNews.getUrl());
                    similarNewsList.add(similarNews);
                }
            });
            logger.debug("#getSimilarNewsList Similar News size for ", news.getUrl(), ":", similarNewsList.size());
        }
        return similarNewsList;
    }
}
