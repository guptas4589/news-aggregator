package com.skg.news_aggregator.search;

public class InvalidQueryException extends Exception {
    public InvalidQueryException() {
        super();
    }
    public InvalidQueryException(String message) {
        super(message);
    }
}
