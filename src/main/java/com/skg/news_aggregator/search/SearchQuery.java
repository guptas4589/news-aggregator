package com.skg.news_aggregator.search;

public class SearchQuery {
    String searchText;

    public String getSearchText() {
        return searchText;
    }

    public void setSearchText(String searchText) {
        this.searchText = searchText;
    }
}
