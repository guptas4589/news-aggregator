package com.skg.news_aggregator.search;

import com.skg.news_aggregator.news.NewsService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
@RequestMapping("/news-search")
public class NewsSearchController {
    @Autowired
    NewsSearchService newsSearchService;

    @Autowired
    NewsService newsService;

    Logger logger = LogManager.getLogger();

    @RequestMapping(value = "")
    public ModelAndView index(Model model) {
        model.addAttribute("newsList", newsService.findAll());
        return new ModelAndView("search/index");
    }

    @RequestMapping(value = "/results", method = RequestMethod.GET)
    public ModelAndView search(SearchQuery query, Model model) {
        logger.info("#search searching news with query string" + query.searchText);
        model.addAttribute("searchText", query.searchText );

        try {
            NewsSearchResult searchResult = newsSearchService.searchNewsAndFindSimilar(query.searchText);
            model.addAttribute("newsSearchResult", searchResult);
            logger.info("#search search resulted in " + searchResult.getEntry().size() + " matches");
        } catch (SearchFailedException e) {
            logger.error("#search search failed.", e);
            model.addAttribute("errorMessage", "Could not service your request please try again later.");
        } catch (InvalidQueryException e) {
            logger.error("#search invalid query.", e);
            model.addAttribute("errorMessage", "Your query was invalid. Please try sommething else");
        }
        return new ModelAndView("/search/search-results");
    }
}
