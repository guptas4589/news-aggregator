package com.skg.news_aggregator.search;

import com.skg.news_aggregator.news.NewsService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;


@Controller
@RequestMapping("/similar-news")
public class SimilarNewsController {
    @Autowired
    NewsSearchService newsSearchService;
    @Autowired
    NewsService newsService;

    Logger logger = LogManager.getLogger();

    @RequestMapping(value = "/{newsId}")
    public ModelAndView search(@PathVariable("newsId") long id, String originalSearchText, Model model) {
        logger.info("#search searching similar new with Id {}", id);
         try{
            model.addAttribute("originalSearchText", originalSearchText);
            model.addAttribute("mainNews", newsService.findById(id));
            model.addAttribute("similarNewsList", newsSearchService.getSimilarNews(id));
        } catch (SearchFailedException e) {
            logger.error("#search search failed.", e);
            model.addAttribute("errorMessage", "Could not service your request please try again later.");
        } catch (InvalidQueryException e) {
            logger.error("#search invalid query.", e);
            model.addAttribute("errorMessage", "Your query was invalid. Please try something else");
        }
        return new ModelAndView("/search/similar-news");
    }
}
