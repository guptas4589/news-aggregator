package com.skg.news_aggregator.search;

public class SearchFailedException extends Exception {
    public SearchFailedException() {
        super();
    }
    public SearchFailedException(String message) {
        super(message);
    }
}
