package com.skg.news_aggregator.search;

import com.skg.news_aggregator.news.News;

public class SearchResultEntry {
    News news;
    Integer similarNewsCount;

    public News getNews() {
        return news;
    }

    public void setNews(News news) {
        this.news = news;
    }

    public Integer getSimilarNewsCount() {
        return similarNewsCount;
    }

    public void setSimilarNewsCount(Integer similarNewsCount) {
        this.similarNewsCount = similarNewsCount;
    }
}
