package com.skg.news_aggregator.rss;

import com.rometools.rome.feed.synd.SyndEntry;
import com.rometools.rome.feed.synd.SyndFeed;
import com.rometools.rome.io.FeedException;
import com.rometools.rome.io.SyndFeedInput;
import com.rometools.rome.io.XmlReader;

import java.io.IOException;
import java.net.URL;
import java.util.List;

public class RSSExtractor {

    public static List<SyndEntry> getSyndEntries(URL feedUrl) throws IOException {
        SyndFeedInput input = new SyndFeedInput();
        XmlReader xmlReader = new XmlReader(feedUrl);
        try {
            SyndFeed feed = input.build(xmlReader);
            return feed.getEntries();
        } catch (FeedException e) {
            throw new IOException(e);
        } finally {
            xmlReader.close();
        }
    }
}
