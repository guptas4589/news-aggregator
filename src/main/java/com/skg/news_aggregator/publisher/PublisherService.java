package com.skg.news_aggregator.publisher;

import com.skg.news_aggregator.feed.Feed;
import com.skg.news_aggregator.feed.FeedService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

@Service
@Transactional
public class PublisherService {

    @Autowired
    PublisherRepository publisherRepository;

    @Autowired
    FeedService feedService;

    Logger logger = LogManager.getLogger();

    public Publisher save(Publisher publisher) {
        return publisherRepository.save(publisher);
    }

    public Publisher save(String name, String urlString) throws MalformedURLException {
        Publisher publisher = new Publisher();
        publisher.setName(name);
        URL url = new URL(urlString);
        publisher.setUrl(url);
        return publisherRepository.save(publisher);
    }

    public List<Publisher> findAll() {
        return publisherRepository.findAll();
    }

    public Publisher findByUrl(String url) {
        Publisher publisher = publisherRepository.findByUrl(url);
        return publisher;
    }

    public void addFeed(Publisher publisher, Feed feed) {
        publisherRepository.addFeed(publisher, feed);
    }

    public void addFeed(Publisher publisher, String feedUrl) {
        try {
            URL url = new URL(feedUrl);
            Feed feed = feedService.save(url, publisher);
            publisher.addToFeedList(feed);
            publisherRepository.update(publisher);
        } catch (MalformedURLException e) {
            logger.warn("#addFeed failed to add feed. malformed url:" + feedUrl);
        }
    }

}
