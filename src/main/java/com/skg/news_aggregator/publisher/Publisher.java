package com.skg.news_aggregator.publisher;

import com.skg.news_aggregator.feed.Feed;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

@Entity(name = "publisher")
public class Publisher {

    @Id @GeneratedValue
    Long id;

    @NotNull
    String name;

    @NotNull
    @org.hibernate.annotations.Type( type="java.net.URL" )
    @Column(unique = true)
    URL url;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "publisher", fetch = FetchType.EAGER)
    List<Feed> feedList = new ArrayList<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public URL getUrl() {
        return url;
    }

    public void setUrl(URL url) {
        this.url = url;
    }

    public List<Feed> getFeedList() {
        return feedList;
    }

    public void setFeedList(List<Feed> feedList) {
        this.feedList = feedList;
    }

    public void addToFeedList(Feed feed) {
        this.getFeedList().add(feed);
        feed.setPublisher(this);
    }
}
