package com.skg.news_aggregator.publisher;

import com.skg.news_aggregator.feed.Feed;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.net.URL;
import java.util.List;

@Repository
@Transactional
public class PublisherRepository  {

    @PersistenceContext
    EntityManager entityManager;

    private static Logger logger = LogManager.getLogger();

    public Publisher save(Publisher publisher) {
        return entityManager.merge(publisher);
    }
    public Publisher update(Publisher publisher) {
        if(entityManager.find(Publisher.class, publisher.getId()) != null) {
            return entityManager.merge(publisher);
        } else {
            logger.error("Publisher not found" + publisher.getId());
        }
        return publisher;
    }

    public Publisher find(Long id) {
        return entityManager.find(Publisher.class, id);
    }

    public Publisher findByUrl(String urlString) {
        URL url = null;
        try {
            url = new URL(urlString);
            Query query = entityManager.createQuery("SELECT distinct(p) FROM publisher p where p.url = :publisherUrl")
                    .setParameter("publisherUrl", url);
                return (Publisher) query.getSingleResult();

        } catch (Exception e) {
            logger.error("#findByURl Exception", e);
        }
        return null;
    }

    public List<Publisher> findAll() {
        Query query = entityManager.createQuery("SELECT e FROM publisher e");
        return (List<Publisher>) query.getResultList();
    }

    public void addFeed(Publisher publisher,Feed feed) {
        publisher.getFeedList().add(feed);
        feed.setPublisher(publisher);
        entityManager.merge(publisher);
    }
}