package com.skg.news_aggregator.util;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.IOException;


public class HtmlParser {
    private static Logger logger = LogManager.getLogger();
    public static String getBodyText(String url) throws IOException {
        logger.info("#getBodyText  parsing html content from {}", url);
        Document doc = Jsoup.connect(url).get();
        logger.info(doc.title());
        return doc.body().text();
    }
}
