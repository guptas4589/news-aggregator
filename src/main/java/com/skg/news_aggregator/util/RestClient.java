package com.skg.news_aggregator.util;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import java.util.Map;

public class RestClient {
    private static Logger logger = LogManager.getLogger();

    public static ResponseEntity<String> get(String url) {
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<String> res = restTemplate.getForEntity(url, String.class);
        logger.debug(res);
        return res;
//        assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));
    }
    public static ResponseEntity<String> get(String url, Map<String, String> params) {
        RestTemplate restTemplate = new RestTemplate();
        String getUrl = getResourceUrl(url, params);
        ResponseEntity<String> res = restTemplate.getForEntity(getUrl, String.class);
        logger.debug(res);
        return res;
//        assertThat(response.getStatusCode(), equalTo(HttpStatus.OK));
    }

    public static <T> T get(String url, Map<String, String> params, Class<T> clazz) {
        RestTemplate restTemplate = new RestTemplate();
        String getUrl = getResourceUrl(url, params);
        return restTemplate
                .getForObject(getUrl, clazz);
    }

    public static String getResourceUrl(String url, Map<String, String> params) {
        String getUrl = url;
        if(params != null && params.size() > 0) {
            getUrl += "?";
            for(Map.Entry<String, String> param: params.entrySet()) {
                getUrl += (param.getKey() + "=" + param.getValue());
            }
        }
        return getUrl;
    }
}
