package com.skg.news_aggregator;

import com.skg.news_aggregator.feed.FeedService;
import com.skg.news_aggregator.index.Index;
import com.skg.news_aggregator.news.News;
import com.skg.news_aggregator.news.NewsIndexService;
import com.skg.news_aggregator.news.NewsService;
import com.skg.news_aggregator.publisher.Publisher;
import com.skg.news_aggregator.publisher.PublisherService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.RAMDirectory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.web.WebApplicationInitializer;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ApplicationStartup implements ApplicationListener<ContextRefreshedEvent>, WebApplicationInitializer {
    @Autowired
    PublisherService publisherService;
    @Autowired
    NewsService newsService;

    @Autowired
    FeedService feedService;

    private static Logger logger = LogManager.getLogger();

    @Override
    public void onStartup(ServletContext servletContext)
            throws ServletException {

        Analyzer analyzer = new StandardAnalyzer();
        Directory directory = new RAMDirectory();
        try {
            Index index = new Index(analyzer, directory);
            servletContext.setAttribute(NewsIndexService.indexName, index);
        } catch (IOException e) {
            logger.error("#onStartup Error initializing news Index");
            ServletException exception = new ServletException();
            exception.initCause(e);
            throw exception;
        }
    }

    @Override
    public void onApplicationEvent(ContextRefreshedEvent event) {
        savePublishers();
        savePublisherFeeds();
        try {
            loadIndex();
        } catch (IOException e) {
            logger.warn("failed to load index news" );
        }
    }

    private void loadIndex() throws IOException {
        List<News> newsList = newsService.findAll();
        newsService.addToIndex(newsList);
    }

    public void savePublishers() {
        Map<String, String> publisherInfo = getPublisherInfo();
        publisherInfo.entrySet().stream().forEach((entry) -> {
            try {
                if(publisherService.findByUrl(entry.getKey()) == null) {
                    publisherService.save(entry.getValue(), entry.getKey());
                }
            }catch (MalformedURLException e) {
                logger.warn("Malformed publisher url" + entry.getValue());
            }
        });
    }

    public void savePublisherFeeds() {
        Map<String, List<String>> publisherFeedInfo = getPublisherFeedInfo();
        for (Map.Entry<String, List<String>> entry : publisherFeedInfo.entrySet())
        {
            Publisher publisher = publisherService.findByUrl(entry.getKey());
            for(String feedUrl: entry.getValue()) {
                if(feedService.find(publisher, feedUrl).isEmpty()) {
                    publisherService.addFeed(publisher, feedUrl);
                }
            }
        }
    }

    public static Map<String, String> getPublisherInfo() {
        Map<String, String> publisherInfo = new HashMap<>();
        publisherInfo.put("https://www.huffingtonpost.com", "Huffington post");
        publisherInfo.put("https://www.nytimes.com","NewYork Times");
        publisherInfo.put("https://www.wsj.com","Wall Street Journal");
        return publisherInfo;
    }

    Map<String, List<String>> getPublisherFeedInfo() {
        Map<String, List<String>> publisherFeedInfo = new HashMap<>();
        List<String> feeds = Arrays.asList("https://www.huffingtonpost.com/section/front-page/feed", "https://www.huffingtonpost.com/section/politics/feed");
        publisherFeedInfo.put("https://www.huffingtonpost.com", feeds);

        feeds = Arrays.asList("http://rss.nytimes.com/services/xml/rss/nyt/HomePage.xml");
        publisherFeedInfo.put("https://www.nytimes.com", feeds);

        feeds = Arrays.asList("http://online.wsj.com/xml/rss/3_7085.xml");
        publisherFeedInfo.put("https://www.wsj.com", feeds);
        return publisherFeedInfo;
    }

}
 