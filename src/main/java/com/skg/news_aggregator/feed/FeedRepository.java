package com.skg.news_aggregator.feed;

import com.skg.news_aggregator.publisher.Publisher;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.net.URL;
import java.util.List;

@Repository
@Transactional
public class FeedRepository {
    @PersistenceContext
    EntityManager entityManager;

    public Feed save(Feed feed) {
        return entityManager.merge(feed);
    }

    public Feed save(URL url, Publisher publisher) {
        Feed feed = new Feed();
        feed.setUrl(url);
        feed.setPublisher(publisher);
        return save(feed);
    }

    public Feed findAll(Long id) {
        return entityManager.find(Feed.class, id);
    }

    public List<Feed> findAll(Publisher publisher) {
        Query query = entityManager.createQuery("SELECT f FROM feed f where f.publisher = :publisher")
                .setParameter("publisher", publisher);
        return (List<Feed>)query.getResultList();
    }

    public List<Feed> findAll(Publisher publisher, URL url) {
        Query query = entityManager.createQuery("SELECT f FROM feed f where f.publisher = :publisher and f.url = :url")
                .setParameter("publisher", publisher)
                .setParameter("url", url);
        return (List<Feed>)query.getResultList();
    }

    public List<Feed> findAll() {
        Query query = entityManager.createQuery("SELECT e FROM Feed e");
        return (List<Feed>) query.getResultList();
    }
}
