package com.skg.news_aggregator.feed;

import com.skg.news_aggregator.news.NewsService;
import com.skg.news_aggregator.publisher.Publisher;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
public class FeedService {

    @Autowired
    FeedRepository feedRepository;

    @Autowired
    NewsService newsService;

    @Autowired
    FeedNewsExtractorService feedNewsExtractorService;

    private static Logger logger = LogManager.getLogger();


    public Feed save(Feed feed) {
        return feedRepository.save(feed);
    }

    public Feed save(URL url, Publisher publisher) {
        return feedRepository.save(url, publisher);
    }

    public List<Feed> find(Publisher publisher) {
        return feedRepository.findAll(publisher);
    }

    public List<Feed> find(Publisher publisher, String urlString) {
        List<Feed> feedList = new ArrayList<>();
        try {
            URL url = new URL(urlString);
            feedList = feedRepository.findAll(publisher, url);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        return feedList;
    }
}
