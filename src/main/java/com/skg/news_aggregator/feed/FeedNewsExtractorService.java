package com.skg.news_aggregator.feed;

import com.rometools.rome.feed.synd.SyndEntry;
import com.skg.news_aggregator.news.News;
import com.skg.news_aggregator.news.NewsService;
import com.skg.news_aggregator.rss.RSSExtractor;
import com.skg.news_aggregator.util.HtmlParser;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
public class FeedNewsExtractorService {

    private static Logger logger = LogManager.getLogger();

    @Autowired
    NewsService newsService;

    public List<News> createNews(Feed feed) throws IOException {
            List<SyndEntry> feedEntries = RSSExtractor.getSyndEntries(feed.getUrl());
            List<News> newsList = new ArrayList<>();
            feedEntries.stream().forEach(syndEntry -> {
                if(!newsService.exists(syndEntry.getLink())) {
                    try {
                        String bodyContent = HtmlParser.getBodyText(syndEntry.getLink());
                        News news = createNews(syndEntry);
                        news.setPublisher(feed.getPublisher());
                        news.setContent(bodyContent);
                        newsList.add(news);
                        try {
                            Thread.sleep(1000);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    } catch (IOException e) {
                        logger.warn("#searchNews Failed to retrieve feed content from {} for {}. Error:"
                                ,syndEntry.getLink(), feed, e.getMessage());
                    }
                }
            });
        return newsList;
    }

    public News createNews(SyndEntry syndEntry) throws MalformedURLException {
        News news = new News();
        news.setDescription(syndEntry.getDescription().getValue());
        news.setTitle(syndEntry.getTitle());
        news.setDate(syndEntry.getPublishedDate());
        news.setUrl(new URL(syndEntry.getLink()));
        return news;
    }
}
