package com.skg.news_aggregator.feed;

import com.skg.news_aggregator.publisher.Publisher;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;
import java.net.URL;

@Entity(name = "feed")
public class Feed {

    @Id @GeneratedValue
    Long id;

    String category;

    @NotNull
    @org.hibernate.annotations.Type( type="java.net.URL" )
    URL url;

    @ManyToOne @NotNull
    Publisher publisher;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public URL getUrl() {
        return url;
    }

    public void setUrl(URL url) {
        this.url = url;
    }

    public Publisher getPublisher() {
        return publisher;
    }

    public void setPublisher(Publisher publisher) {
        this.publisher = publisher;
    }

    public String toString() {
        return getId() + ":" + getUrl();
    }
}
