package com.skg.news_aggregator;

import com.skg.news_aggregator.news.NewsService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class HomeController {

    @Autowired
    NewsService newsService;

    Logger logger = LogManager.getLogger();

    @RequestMapping("/")
    public ModelAndView index(Model model) {
        logger.info("#index showing home page");
        model.addAttribute("newsList", newsService.findAll());
        return new ModelAndView("search/index");
    }
}
