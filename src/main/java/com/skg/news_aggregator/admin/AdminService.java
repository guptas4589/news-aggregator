package com.skg.news_aggregator.admin;

import com.skg.news_aggregator.feed.Feed;
import com.skg.news_aggregator.feed.FeedService;
import com.skg.news_aggregator.news.NewsService;
import com.skg.news_aggregator.publisher.PublisherService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class AdminService {
    @Autowired
    PublisherService publisherService;

    @Autowired
    FeedService feedService;

    @Autowired
    NewsService newsService;

    Logger logger = LogManager.getLogger();

    public void syncNewsFromFeed() {
        publisherService.findAll().stream().forEach((publisher -> {
            logger.info("#syncFromFeed syncing feed from publisher: " + publisher.getName());
            List<Feed> feedList = feedService.find(publisher);
            newsService.updateNews(feedList);
        }));
        logger.info("#syncNewsFromFeed sync complete");
    }
}
