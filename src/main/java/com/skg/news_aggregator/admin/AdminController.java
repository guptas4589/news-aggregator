package com.skg.news_aggregator.admin;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/admin")
public class AdminController {

    @Autowired
    AdminService adminService;

    @RequestMapping("/sync-feed")
    public String save() {
        adminService.syncNewsFromFeed();
        return "success";
    }
}
