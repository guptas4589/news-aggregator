<%--
  Created by IntelliJ IDEA.
  User: satish
  Date: 12/19/17
  Time: 1:19 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>$Title$</title>
    <link href="<c:url value="/resources/styles/style.css" />" rel="stylesheet">
</head>
<body>
<div class="search-form">
    <form:form method="get" action="/news-search/results" modelAttribute="searchQuery" >
        <input type="text" name="searchText" placeholder="Search text" value="${searchText}"/>
        <input type="submit" value="Search"/>
    </form:form>
</div>

<p>Showing resuls for: ${searchText}</p>
<div>
    <c:forEach items="${newsSearchResult.entry}" var="searchResult">
    <div class="news-search-item">
        <c:set var="news" value="${searchResult.news}"/>
        <div class="news-summary">
            <a href="${news.url}" class="title">${news.title}</a>
            <p class="description">${news.description}</p>
            <p class="link"><a href="${news.url}">${news.url}</a></p>
                <%--<p>${news.content.substring(0, news.content.length() % 1000)}</p>--%>
            <div>
                <p><a href="/similar-news/${news.id}?originalSearchText=${searchText}">${searchResult.similarNewsCount} similar</a></p>
            </div>
        </div>
    </div>
    </c:forEach>
</div>
</body>

</html>
