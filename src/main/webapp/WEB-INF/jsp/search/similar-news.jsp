<%--
  Created by IntelliJ IDEA.
  User: satish
  Date: 12/19/17
  Time: 1:19 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>$Title$</title>
    <link href="<c:url value="/resources/styles/style.css" />" rel="stylesheet">
</head>
<body>
<div>
    <div>
        <p>Original search: ${originalSearchText}</p>
        <p>Original News</p>
        <div class="news-summary">
            <a href="${mainNews.url}" class="title">${mainNews.title}</a>
            <p class="description">${mainNews.description}</p>
            <p class="link"><a href="${mainNews.url}">${mainNews.url}</a></p>
        </div>
    </div>
    <div>
        <div>
            <p>Similar News:</p>
        </div>
        <c:forEach items="${similarNewsList}" var="news">
            <div class="news-search-item">
                <div class="news-summary">
                    <a href="${news.url}" class="title">${news.title}</a>
                    <p class="description">${news.description}</p>
                    <p class="link"><a href="${news.url}">${news.url}</a></p>
                    <p><a href="/similar-news/${news.id}">${searchResult.similarNewsCount} similar</a></p>
                </div>
            </div>
        </c:forEach>
    </div>
</div>
</body>
</html>
