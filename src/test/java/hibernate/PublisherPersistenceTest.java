package hibernate;

import com.skg.news_aggregator.feed.Feed;
import com.skg.news_aggregator.publisher.Publisher;
import org.junit.Test;

import java.util.ArrayList;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

public class PublisherPersistenceTest {

    @Test
    public void publisherIsSaved() {
        Publisher publisher = new Publisher();
        TransactionWrapper transactionWrapper = new TransactionWrapper();
        GenericDAO<Publisher> genericDAO = new GenericDAO<>(transactionWrapper.getSession());
        transactionWrapper.beginTransaction();
        publisher.setName("NewYork Times");
        assertNull(publisher.getId());
        genericDAO.insert(publisher);
        assertNotNull(publisher.getId());
        transactionWrapper.commit();
    }

    @Test
    public void publisherFeedsIsSaved() {
        Feed feed1 = new Feed();
        feed1.setCategory("category1");
        feed1.setUrl("http://test.com");
        Feed feed2 = new Feed();
        feed2.setCategory("http://test2.com");

        Publisher publisher = new Publisher();
        ArrayList<Feed> arrayList = new ArrayList<>();
        arrayList.add(feed1);
        arrayList.add(feed2);
        publisher.setFeedList(arrayList);
        feed1.setPublisher(publisher);
        feed2.setPublisher(publisher);

        TransactionWrapper txnWrapper = new TransactionWrapper();
        GenericDAO<Publisher> publisherDao = new GenericDAO<>(txnWrapper.getSession());
        txnWrapper.beginTransaction();
        publisherDao.insert(publisher);
        txnWrapper.commit();

        txnWrapper = new TransactionWrapper();
        publisherDao.setSession(txnWrapper.getSession());
        txnWrapper.beginTransaction();
        publisherDao.refresh(publisher);

        GenericDAO<Feed> feedDao = new GenericDAO<>(txnWrapper.getSession());
        feedDao.refresh(feed1);
        feedDao.refresh(feed1);
        txnWrapper.commit();

        assertNotNull(publisher.getFeedList().get(0).getId());
        assertNotNull(publisher.getFeedList().get(1).getId());

        assertNotNull(feed1.getPublisher());
        assertNotNull(feed2.getPublisher());

        assertTrue(feed1.getPublisher().getId().equals(publisher.getId()));
        assertTrue(feed2.getPublisher().getId().equals(publisher.getId()));
    }
}
