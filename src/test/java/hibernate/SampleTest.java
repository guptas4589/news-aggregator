package hibernate;

import com.skg.news_aggregator.publisher.Publisher;
import org.junit.Test;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

public class SampleTest {

    @Test
    public void sampleTest() {
        Publisher publisher = new Publisher();
        TransactionWrapper transactionWrapper = new TransactionWrapper();
        GenericDAO<Publisher> genericDAO = new GenericDAO<>(transactionWrapper.getSession());
        transactionWrapper.beginTransaction();
        publisher.setName("NewYorkTimes");
        assertNull(publisher.getId());
        genericDAO.insert(publisher);
        assertNotNull(publisher.getId());
        transactionWrapper.commit();
    }
}
